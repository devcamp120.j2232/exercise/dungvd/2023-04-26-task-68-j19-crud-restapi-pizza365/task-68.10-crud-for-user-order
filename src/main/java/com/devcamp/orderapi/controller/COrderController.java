package com.devcamp.orderapi.controller;


import java.util.Date;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.orderapi.model.COrder;
import com.devcamp.orderapi.model.CUser;
import com.devcamp.orderapi.repository.IOrderRepository;
import com.devcamp.orderapi.repository.IUserRepository;

@RestController
@CrossOrigin
public class COrderController {
    @Autowired
    IOrderRepository pIOrderRepository;

    @Autowired
    IUserRepository pIUserRepository;
    //API lấy ra toàn bộ order
    @GetMapping("/orders")
    public ResponseEntity<Object> getAllOrders() {
        try {
            return new ResponseEntity<>(pIOrderRepository.findAll(), HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
            
        }
    }

    // API lấy ra order thông qua User ID
    @GetMapping("/user/{id}/orders")
    public ResponseEntity<Set<COrder>> getOrdersByUserId(@PathVariable("id") long id) {
        try {
            CUser user = pIUserRepository.findById(id).get();
            return new ResponseEntity<>(user.getOrders(), HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);

        }
    }

    // API lấy ra oder by Order id
    @GetMapping("order/{id}")
    public ResponseEntity<COrder> getOrderById(@PathVariable("id") long id) {
        try {
            COrder order = pIOrderRepository.findById(id).get();
            return new ResponseEntity<>(order, HttpStatus.OK);

        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

     //API tạo mới 1 order
     @PostMapping("/user/{id}")
     public ResponseEntity<COrder> createOrder(@RequestBody COrder pOrder, @PathVariable("id") long id) {
         try {
             COrder order = new COrder();
             CUser user = pIUserRepository.findById(id).get();
             order.setOrderCode(pOrder.getOrderCode());
             order.setPizzaSize(pOrder.getPizzaSize());
             order.setPizzaType(pOrder.getPizzaType());
             order.setPaid(pOrder.getPaid());
             order.setPrice(pOrder.getPrice());
             order.setUser(user);
             order.setVoucherCode(pOrder.getVoucherCode());
             order.setNgayTao(new Date());
             order.setNgayCapNhat(null);
             COrder savedUser = pIOrderRepository.save(order);
             return new ResponseEntity<>(savedUser, HttpStatus.OK);
         } catch (Exception e) {
             return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
         }
     }
 
     // API Sửa thông tin 1 order
     @PutMapping("/order/{id}")
     public ResponseEntity<COrder> updateOrder(@PathVariable("id") long id, @RequestBody COrder pOrder) {
         try {
            COrder order = pIOrderRepository.findById(id).get();
            order.setOrderCode(pOrder.getOrderCode());
            order.setPizzaSize(pOrder.getPizzaSize());
            order.setPizzaType(pOrder.getPizzaType());
            order.setPaid(pOrder.getPaid());
            order.setPrice(pOrder.getPrice());
            order.setUser(pOrder.getUser());
            order.setVoucherCode(pOrder.getVoucherCode());
            order.setNgayCapNhat(null);
            COrder savedUser = pIOrderRepository.save(order);
            return new ResponseEntity<>(savedUser, HttpStatus.OK);
         } catch (Exception e) {
             return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
         }
     }
 
 
     //API delete 1 order
     @DeleteMapping("/order/{id}")
     public ResponseEntity<COrder> deleteOrder(@PathVariable("id") long id) {
         try {
             pIOrderRepository.deleteById(id);
             return new ResponseEntity<>(HttpStatus.NO_CONTENT);
         } catch (Exception e) {
             return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
         }
 
     }

}
