package com.devcamp.orderapi.controller;


import java.util.Date;
import java.util.List;
import java.util.Optional;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.orderapi.model.CUser;
import com.devcamp.orderapi.repository.IUserRepository;

@RestController
@CrossOrigin
public class CUserController {
    @Autowired
    IUserRepository pIUserRepository;

    //API load ra toàn bộ user
    @GetMapping("/users")
    public ResponseEntity<List<CUser>> getAllUser() {
        try {
            return new ResponseEntity<>(pIUserRepository.findAll(), HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    // API lấy thông tin user thông qua ID
    @GetMapping("/user/{id}")
    public ResponseEntity<CUser> getUserById(@PathVariable("id") long id) {
        try {
            Optional<CUser> userData = pIUserRepository.findById(id);
            if(userData.isPresent()) {
                CUser user = userData.get();
                return new ResponseEntity<>(user, HttpStatus.OK);
            }
            else {
                return new ResponseEntity<>(HttpStatus.NOT_FOUND);
            }
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    //API tạo mới 1 user 
    @PostMapping("/users")
    public ResponseEntity<CUser> createUser(@RequestBody CUser pUser) {
        try {
            CUser user = new CUser();
            user.setFullname(pUser.getFullname());
            user.setAddress(pUser.getAddress());
            user.setEmail(pUser.getEmail());
            user.setPhone(pUser.getPhone());
            user.setOrders(pUser.getOrders());
            user.setNgayTao(new Date());
            user.setNgayCapNhat(null);
            CUser savedUser = pIUserRepository.save(user);
            return new ResponseEntity<>(savedUser, HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    // API Sửa thông tin 1 user
    @PutMapping("/user/{id}")
    public ResponseEntity<CUser> updateUser(@PathVariable("id") long id, @RequestBody CUser pUser) {
        try {
            CUser user = pIUserRepository.findById(id).get();
            user.setFullname(pUser.getFullname());
            user.setAddress(pUser.getAddress());
            user.setEmail(pUser.getEmail());
            user.setPhone(pUser.getPhone());
            user.setOrders(pUser.getOrders());
            user.setNgayCapNhat(new Date());
            CUser savedUser = pIUserRepository.save(user);
            return new ResponseEntity<>(savedUser, HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }


    //API delete 1 user
    @DeleteMapping("/user/{id}")
    public ResponseEntity<CUser> deleteUser(@PathVariable("id") long id) {
        try {
            pIUserRepository.deleteById(id);
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }

    }

}
