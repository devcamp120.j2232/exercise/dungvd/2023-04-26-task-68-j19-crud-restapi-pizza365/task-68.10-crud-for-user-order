package com.devcamp.orderapi.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.devcamp.orderapi.model.CUser;

public interface IUserRepository extends JpaRepository<CUser, Long>{
    
}
