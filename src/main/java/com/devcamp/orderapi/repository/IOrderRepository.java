package com.devcamp.orderapi.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.devcamp.orderapi.model.COrder;

public interface IOrderRepository extends JpaRepository<COrder, Long>{
    
}
